hCaptcha for Drupal
====================

The hCaptcha module uses the hCaptcha web service to
improve the CAPTCHA system and protect email addresses. For
more information on what hCaptcha is, please visit:
    https://hcaptcha.com/

DEPENDENCIES
------------

* hCaptcha module depends on the CAPTCHA module.
  https://drupal.org/project/captcha


CONFIGURATION
-------------

1. Enable hCaptcha and CAPTCHA modules on:
       admin/modules

2. You'll now find a hCaptcha tab on the CAPTCHA
   administration page available at:
       admin/config/people/captcha/hcaptcha

3. Register on:
       https://hcaptcha.com/

4. Input the site and secret keys into the hCaptcha settings.

5. Visit the Captcha administration page and set where you
   want the hCaptcha form to be presented:
       admin/config/people/captcha

THANK YOU
---------

 * Thank you goes to the hCaptcha team for all their
   help, support and their amazing Captcha solution
       https://hcaptcha.com/
