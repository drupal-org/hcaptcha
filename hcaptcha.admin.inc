<?php

/**
 * Administrative settings for hCaptcha.
 */
function hcaptcha_admin_settings() {
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['general_settings']['hcaptcha_site_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Site key'),
    '#default_value' => variable_get('hcaptcha_site_key', ''),
    '#maxlength' => 50,
    '#description' => t('The site key given to you when you <a href="@url" target="_blank">register for hCaptcha</a>.', array('@url' => 'https://hcaptcha.com/?r=8a46bae6b225')),
    '#required' => true,
  );
  $form['general_settings']['hcaptcha_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret key'),
    '#default_value' => variable_get('hcaptcha_secret_key', ''),
    '#maxlength' => 50,
    '#description' => t('The secret key given to you when you <a href="@url" target="_blank">register for hCaptcha</a>.', array('@url' => 'https://hcaptcha.com/?r=8a46bae6b225')),
    '#required' => true,
  );
  $form['general_settings']['hcaptcha_src'] = array(
    '#type' => 'textfield',
    '#title' => t('hCaptcha javascript resource URL'),
    '#default_value' => variable_get('hcaptcha_src', ''),
    '#maxlength' => 200,
    '#description' => t('Default URL is "@url".', array('@url' => 'https://hcaptcha.com/1/api.js')),
    '#required' => true,
  );

  $form['widget_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget settings'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $form['widget_settings']['hcaptcha_theme'] = array(
    '#title' => t('Theme'),
    '#description' => t('Defines which theme to use for hCaptcha.'),
    '#type' => 'select',
    '#options' => array(
      '' => t('Light (default)'),
      'dark' => t('Dark'),
    ),
    '#default_value' => variable_get('hcaptcha_theme', ''),
  );
  $form['widget_settings']['hcaptcha_size'] = array(
    '#title' => t('Size'),
    '#description' => t('The size of hCaptcha widget.'),
    '#type' => 'select',
    '#options' => array(
      '' => t('Normal (default)'),
      'compact' => t('Compact'),
    ),
    '#default_value' => variable_get('hcaptcha_size', ''),
  );
  $form['widget_settings']['hcaptcha_tabindex'] = array(
    '#type' => 'textfield',
    '#title' => t('Tabindex'),
    '#description' => t('Set the <a href="@tabindex">tabindex</a> of the widget and challenge (Default = 0). If other elements on your page use tabindex, it should be set to make user navigation easier.', array('@tabindex' => 'https://www.w3.org/TR/html4/interact/forms.html#adef-tabindex')),
    '#default_value' => variable_get('hcaptcha_tabindex', 0),
    '#size' => 4,
  );

  return system_settings_form($form);
}

/**
 * Validation function for hcaptcha_admin_settings().
 *
 * @see hcaptcha_admin_settings()
 */
function hcaptcha_admin_settings_validate($form, &$form_state) {
  $tabindex = $form_state['values']['hcaptcha_tabindex'];
  if (!is_numeric($tabindex)) {
    form_set_error('hcaptcha_tabindex', t('The tabindex must be an integer.'));
  }
}
